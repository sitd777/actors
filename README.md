Actors Test Project
------------

- PHP framework: Phalcon
- CSS framework: Bootstrap
- JS framework: Backbone, Underscore, jQuery, jQuery UI


DB configuration
------------
Db type: MySQL
~~~
username - root
password - [empty]
dbname   - actors
~~~

Backend API URL's:
------------
~~~
/api/list - full list of actors
/api/list/[search word] - filtered list of actors
~~~

Frontend URL's:
------------
~~~
/index/index - list of actors
/index/view/[id] - page of actor
~~~
