<?php

$router = $di->getRouter();

$router->addGet('#^/api/([a-z0-9]+)(/?.*)$#', [
    'controller' => 'api',
    'action'     => 1,
    'params'     => 2
]);

$router->handle();
