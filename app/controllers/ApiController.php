<?php

class ApiController extends ControllerBase
{
    /**
     * Limit of records on page
     * @var int
     */
    protected $pageLimit = 20;

    /**
     * Returns list of actors
     * @param null $q
     */
    public function listAction($q = null)
    {
        $q = trim($q);
        if (empty($q)) {
            $phql = 'SELECT * FROM Actors ORDER BY id ASC LIMIT ' . $this->pageLimit;
            $actors = $this->modelsManager->executeQuery($phql);
        } else {
            $phql = 'SELECT * FROM Actors WHERE name LIKE :name: OR aliases LIKE :name: ORDER BY id ASC LIMIT ' . $this->pageLimit;
            $actors = $this->modelsManager->executeQuery(
                $phql,
                [
                    'name' => '%' . $q . '%',
                ]
            );
        }

        $data = [];
        foreach ($actors as $actor) {
            $data[] = $actor->toArray();
        }

        echo json_encode($data);
        die;
    }

    /**
     * Returns selected actor data
     * @param $id
     */
    public function getAction($id)
    {
        $phql = 'SELECT * FROM Actors WHERE id = :id:';
        $data = [
            'result' => 'false',
            'data' => []
        ];

        $actor = $this->modelsManager->executeQuery(
            $phql,
            [
                'id' => $id,
            ]
        )->getFirst();
        if($actor) {
            $data['result'] = 'true';
            $data['data'] = $actor->toArray();
        }

        echo json_encode($data);
        die;
    }
}

