<?php

class IndexController extends ControllerBase
{
    /**
     * Shows list of actors
     */
    public function indexAction()
    {

    }

    /**
     * Show page of actor
     * @param $id
     */
    public function viewAction($id)
    {
        $this->view->setVar("id", $id);
    }
}

