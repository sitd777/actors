<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class DataMigration_102
 */
class DataMigration_102 extends Migration
{
    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        self::$_connection->insert(
            'actors',
            [
                'Том Хэнкс',
                'Thomas Jeffrey «Tom» Hanks',
                '1956-07-09',
                'USA',
                'active',
                'green',
                'braun',
                '90-60-90',
                '["twitter", "icq"]',
                '["https://st.kp.yandex.net/images/actor_iphone/iphone360_9144.jpg", "https://www.film.ru/sites/default/files/people/1457915-877144.jpg", "https://images4.cosmopolitan.ru/upload/img_cache/acd/acd1c0eb45a14d470cfc029c854a5bcc_cropped_460x460.jpg"]',
            ],
            [
                'name',
                'aliases',
                'birthdate',
                'country',
                'career',
                'eyes_color',
                'hair_color',
                'sizes',
                'socials',
                'pictures',
            ]
        );

        self::$_connection->insert(
            'actors',
            [
                'Том Круз',
                'Tom Cruise, Томас Круз Мапотер IV',
                '1967-07-03',
                'USA',
                'active',
                'blue',
                '--',
                'test',
                '["msn"]',
                '["https://st.kp.yandex.net/images/actor_iphone/iphone360_20302.jpg", "http://www.spletnik.ru/img/2017/09/olya/20172709-tomcruise-post-2.jpg", "https://uznayvse.ru/images/celebs/tomkruz_medium.jpg"]',
            ],
            [
                'name',
                'aliases',
                'birthdate',
                'country',
                'career',
                'eyes_color',
                'hair_color',
                'sizes',
                'socials',
                'pictures',
            ]
        );

        self::$_connection->insert(
            'actors',
            [
                'Роберт Дауни младший',
                'Robert John Downey Jr.',
                '1965-04-04',
                'USA',
                'active',
                'blue',
                '--',
                'test',
                '["msn"]',
                '["http://www.sncmedia.ru/upload/iblock/7c2/7c2ba95c3439da9af4a5a313399438f5_w877_h500_crp.jpg", "https://images2.cosmopolitan.ru/upload/img_cache/682/682e52fbc57182aec3b3c9236d075530_fitted_740x0.jpg", "https://fakty.ictv.ua/wp-content/uploads/2017/06/09/Untitled-1-6.jpg"]',
            ],
            [
                'name',
                'aliases',
                'birthdate',
                'country',
                'career',
                'eyes_color',
                'hair_color',
                'sizes',
                'socials',
                'pictures',
            ]
        );

        self::$_connection->insert(
            'actors',
            [
                'Джонни Депп',
                'Джон Кри́стофер Депп II',
                '1963-06-09',
                'USA',
                'active',
                'blue',
                '--',
                'test',
                '["youtube"]',
                '["https://www.film.ru/sites/default/files/people/1456176-915122.jpg", "http://www.multikino.com/img/news/n_17196/0000001460%20(1).jpg", "https://vistanews.ru/uploads/posts/2017-03/1490338694_xcf70dba3.jpg"]',
            ],
            [
                'name',
                'aliases',
                'birthdate',
                'country',
                'career',
                'eyes_color',
                'hair_color',
                'sizes',
                'socials',
                'pictures',
            ]
        );

        for($i = 0; $i < 20; $i++) {
            self::$_connection->insert(
                'actors',
                [
                    'test ' . ($i + 1),
                    '',
                    '1970-01-01',
                    $i % 2 == 0 ? 'USA' : 'Europe',
                    $i % 2 == 0 ? 'active' : 'inactive',
                    $i % 2 == 0 ? 'red' : 'gray',
                    '--',
                    'test',
                    '[]',
                    '[]',
                ],
                [
                    'name',
                    'aliases',
                    'birthdate',
                    'country',
                    'career',
                    'eyes_color',
                    'hair_color',
                    'sizes',
                    'socials',
                    'pictures',
                ]
            );
        }
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

}
