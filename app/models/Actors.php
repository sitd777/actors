<?php

use Phalcon\Mvc\Model;

class Actors extends Model
{
    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=10, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=100, nullable=false)
     */
    public $name;

    /**
     *
     * @var string
     * @Column(column="aliases", type="string", length=100, nullable=false)
     */
    public $aliases;

    /**
     *
     * @var string
     * @Column(column="birthdate", type="string", nullable=false)
     */
    public $birthdate;

    /**
     *
     * @var string
     * @Column(column="country", type="string", length=100, nullable=false)
     */
    public $country;

    /**
     *
     * @var string
     * @Column(column="career", type="string", length=100, nullable=false)
     */
    public $career;

    /**
     *
     * @var string
     * @Column(column="eyes_color", type="string", length=100, nullable=false)
     */
    public $eyes_color;

    /**
     *
     * @var string
     * @Column(column="hair_color", type="string", length=100, nullable=false)
     */
    public $hair_color;

    /**
     *
     * @var string
     * @Column(column="sizes", type="string", length=100, nullable=false)
     */
    public $sizes;

    /**
     *
     * @var string
     * @Column(column="socials", type="string", nullable=false)
     */
    public $socials;

    /**
     *
     * @var string
     * @Column(column="pictures", type="string", nullable=false)
     */
    public $pictures;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("actors");
        $this->setSource("actors");
    }

    public function beforeSave()
    {
        $this->socials = json_encode($this->socials);
        $this->pictures = json_encode($this->pictures);
    }

    public function afterFetch()
    {
        $this->socials = json_decode($this->socials);
        $this->pictures = json_decode($this->pictures);
    }

    public function afterSave()
    {
        $this->socials = json_decode($this->socials);
        $this->pictures = json_decode($this->pictures);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'actors';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actors[]|Actors|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Actors|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
