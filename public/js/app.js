window.ActorsCollection = Backbone.Collection.extend({
    url: URL_API + '/list'
});

window.Actors = new ActorsCollection;

window.ActorsView = Backbone.View.extend({

    tagName:  "div",

    template: $('#item-template').length > 0 ? _.template($('#item-template').html()) : '',

    events: {
        "click .actor" : "actorClick",
    },

    initialize: function() {
        this.model.view = this;
    },

    render: function() {
        $(this.el).html(this.template(this.model.toJSON()));
        return this;
    },

    actorClick: function() {
        var id = $(this.el).find("div.actor").data('id');
        window.location = URL_VIEW + '/' + id;
    },
});

window.AppView = Backbone.View.extend({

    el: $("#actorsapp"),

    statsTemplate: $('#stats-template').length > 0 ? _.template($('#stats-template').html()) : '',

    events: {
        "keyup #q" : "search",
        "click #go" : "search",
    },

    initialize: function() {
        _.bindAll(this, 'addOne', 'render');

        if ($(this.el).data('view') == 'view') {
            var obj = this;
            $.ajax( {
                url: URL_API + '/get/' + $(this.el).data('id'),
                dataType: "json",
                success: function( data ) {
                    if (data.result && $('#view-template').length > 0) {
                        var viewTemplate = _.template($('#view-template').html());
                        var html = viewTemplate({ model : data.data });
                        $(obj.el).html(html);
                    }
                }
            } );
        } else {
            Actors.bind('add', this.addOne);
            Actors.bind('all', this.render);
            Actors.fetch();

            this.$("#q").autocomplete({
                source: function( request, response ) {
                    $.ajax( {
                        url: URL_API + '/list/' + request.term,
                        dataType: "json",
                        success: function( data ) {
                            var results = [];
                            _.each(data, function(dataRow) {
                                var name = dataRow.name.indexOf(request.term) > -1 ? dataRow.name : dataRow.aliases;
                                results.push({id: dataRow.id, value: name});
                            });
                            response( results );
                        }
                    } );
                },
                minLength: 3,
                select: function( event, ui ) {
                    window.location = URL_VIEW + '/' + ui.item.id;
                }
            });
        }
    },

    search: function(event) {
        if ((event.type == 'keyup' && event.originalEvent.keyCode == 13) || event.type == 'click') {
            this.$('#actors').html('<div class="row"> Loading... </div>');
            Actors.reset();
            Actors.url = URL_API + '/list/' + this.$('#q').val();
            Actors.fetch();
        }
    },

    render: function() {
        if(Actors.length == 0) {
            this.$("#actors div").remove();
        }
        this.$('#actors-stats').html(this.statsTemplate({
            total: Actors.length,
        }));
    },

    addOne: function(actor) {
        var view = new ActorsView({model: actor});
        this.$("#actors").append(view.render().el);
    },
});

window.App = new AppView;
